<?
class Validate {
	private function _substr(& $date, $num, $opt = false) {
		if ($opt && strlen($date) >= $opt && preg_match('/^[0-9]{' . $opt . '}/', $date, $m)) {
			$ret = $m[0];
		} else {
			$ret = substr($date, 0, $num);
		}
		$date = substr($date, strlen($ret));
		return $ret;
	}
	/**
	 * Validate a string using the given format 'format'
	 *
	 * @param string    $string     String to validate
	 * @param array     $options    Options array where:
	 *                              'format' is the format of the string
	 *                                  Ex: VALIDATE_NUM . VALIDATE_ALPHA (see constants)
	 *                              'min_length' minimum length
	 *                              'max_length' maximum length
	 *
	 * @return boolean true if valid string, false if not
	 *
	 * @access public
	 */
	public function string($string, $options) {
		$format = null;
		$min_length = $max_length = 0;
		if (is_array($options)) {
			extract($options);
		}
		if (!$string) {
			return false;
		}
		if ($format && !preg_match("|^[$format]*\$|s", $string)) {
			return false;
		}
		if ($min_length && strlen($string) < $min_length) {
			return false;
		}
		if ($max_length && strlen($string) > $max_length) {
			return false;
		}
		return true;
	}

    /**
     * date
     *
     * Valida um campo Date
     *
     * @param string $data
     * @access public
     * @return void
     */
    public function date($data) {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2})$/", $data, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1])) {
                return true;
            }
        }

        return false;

    }


    /**
     * Valida um campo Datetime
     *
     * @return bool
     **/
    public function datetime($data) {
        if (preg_match("/^(\d{4})-(\d{2})-(\d{2}) ([01][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$/", $data, $matches)) {
            if (checkdate($matches[2], $matches[3], $matches[1])) {
                return true;
            }
        }

        return false;

    }

    /**
     * valida um campo Time
     *
     * @return bool
     * @author Me
     **/
    public function time($hora) {
        if (preg_match("/^([01][0-9]|2[0-3]):([0-5][0-9])$/", $hora, $matches))
            return true;
        else
            return false;
    }

}
