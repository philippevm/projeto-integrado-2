<?
class CBase extends CI_Controller {

	public function __construct() {
		parent::__construct();

		if ($this->_checaPaginaExcecao()) {
			$this->iniciaSessao();
			$this->_checaAutenticado();
			$this->_checaPermissao();
		}
	}

	/**
	 * inicia a sessao se ela ja nao estiver iniciada
	 */
	public function iniciaSessao() {
		if (!isset($_SESSION))
			session_start();
	}

	/**
	 * checa se o usuario esta autenticado
	 */
	private function _checaAutenticado() {
		if (!isset($_SESSION['usuario'])) {
			$_SESSION['url'] = current_url();
			redirect(site_url() . '/autenticacao/', 'refresh');
		}
	}

	/**
	 * checa se o usuario tem permissao para acessar a pagina e o metodo
	 */
	private function _checaPermissao() {
		include(APPPATH.'config/permissao.php');
		if (isset($permissao)) {

			$acesso1 = $this->router->fetch_class() . '/' . $this->router->fetch_method();
			$acesso2 = $this->router->fetch_class() . '/*';
			if (!in_array($acesso1, $permissao[$_SESSION['usuario']->perfil]) && !in_array($acesso2, $permissao[$_SESSION['usuario']->perfil])) {
				redirect(site_url() . '/usuario/semPermissao', 'refresh');
			}

		}
		else {
			die('O arquivo de configuracao permissao.php nao foi criado');
		}
	}

	/**
	 * verifica se a pagina atual deve ser checada a autenticacao e permissao
	 *
	 * @return boolean (true - deve ser checada)
	 */
	private function _checaPaginaExcecao() {

		$lista_excecao[] = 'autenticacao/index';
		$lista_excecao[] = 'autenticacao/form';
		$lista_excecao[] = 'autenticacao/login';
		$lista_excecao[] = 'autenticacao/logoff';
		$lista_excecao[] = 'usuario/semPermissao';

		$acesso = $this->router->fetch_class() . '/' . $this->router->fetch_method();
		return in_array($acesso, $lista_excecao) ? false : true;
	}

	/**
	 * monta a listagem de erro do validate
	 *
	 * @param array $lista: lista dos erros retornado pelo validate
	 *
	 * @return string
	 */
	public function validateErro($lista) {
		$this->lang->load('validate');
		$html = '<ul class="error">';
		foreach ($lista as $campo => $valor) {
			if ($valor == false) {
				$msg = $this->lang->line($campo);
				$html .= '<li>' . ($msg ? $msg : "Campo $campo inv&aacute;lido") . '</li>';
			}
		}
		$html .= '</ul>';
		return $html;
	}
}
