<?php
require_once dirname(__FILE__) . '/cbase.php';

class Fornecedores extends CBase {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$dados = array('title' => 'Gerenciamento de Fornecedores');

		$this->template->add_js(js_url() . 'fornecedores/index.js');
		$this->template->write_view('conteudo', 'fornecedores/index', $dados);
		$this->template->render();
	}

	public function ajaxListar () 
	{
		$obj = new Fornecedor();
		$list = $obj->get_datatables();
		$data = array();
		$no = $_POST['start'];
		$string = $this->load->helper('text');

		foreach ($list as $fornecedor) {
			$data[] = array(
				$fornecedor->id,
				$fornecedor->nome,
				mask($fornecedor->telefone, '(##) #####-####'),
				$fornecedor->email,
				mask($fornecedor->cnpj, '###.###.####/##'),
				$this->load->view('fornecedores/botoes', array('id' => $fornecedor->id), true)
			);
			$no++;
		}
		
		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $obj->count_all(),
			"recordsFiltered" => $obj->count_filtered(),
			"data" => $data
		);

		echo json_encode($output);
	}

	public function save () 
	{
		$result = array('status' => false, 'valid' => false, 'errors' => array());
		$obj = new Fornecedor();
		$obj->setFrom($_POST);

		$result['valid'] = $obj->validate();
		if($result['valid']) {
			if ($obj->{$obj->__dm['primaria']}) {
				$result['status'] = $obj->updateSpecific() > 0;
			} else {
				$result['status'] = $obj->create() > 0;
			}
		} else {
			$result['errors'] = validation_errors();
		}

		echo json_encode($result);
	}

	public function get ($id) 
	{	
		$obj = new Fornecedor();
		$data = $obj->get_by_id($id);
		echo json_encode($data);
	}

	public function remove ($id) 
	{
		$obj = new Fornecedor();
		$result = $obj->remove($id);
		echo json_encode(array("status" => $result > 0));
	}
}