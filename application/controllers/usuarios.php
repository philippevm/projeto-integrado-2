<?php
require_once dirname(__FILE__) . '/cbase.php';

class Usuarios extends CBase {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $dados = array('title' => 'Gerenciamento de Usuários');

        $this->template->add_js(js_url() . 'usuarios/index.js');
        $this->template->write_view('conteudo', 'usuarios/index', $dados);
        $this->template->render();
    }

    public function semPermissao() {
        $this->template->write_view('conteudo', 'usuarios/sem_permissao');
        $this->template->render();
    }

    public function ajaxListar ()
    {
        $obj = new Usuario();
        $list = $obj->get_datatables();
        $data = array();
        $no = $_POST['start'];
        $lang = $this->lang->load('dominio_fixo');
        $perfis = $this->lang->language['perfis'];

        foreach ($list as $user) {
            $data[] = array(
                $user->id,
                $user->nome,
                $user->login,
                $user->email,
                $perfis[$user->perfil],
                $this->load->view('usuarios/botoes', array('id' => $user->id), true)
            );
            $no++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $obj->count_all(),
            "recordsFiltered" => $obj->count_filtered(),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function save ()
    {
        $result = array('status' => false, 'valid' => false, 'errors' => array());
        $obj = new Usuario();
        $obj->setFrom($_POST);

        if (!empty($obj->senha)) {
            $obj->senha = md5($obj->senha);
        }

        $result['valid'] = $obj->validate();
        if($result['valid']) {
            if ($obj->{$obj->__dm['primaria']}) {
                $result['status'] = $obj->updateSpecific() > 0;
            } else {
                $result['status'] = $obj->create() > 0;
            }
        } else {
            $result['errors'] = validation_errors();
        }

        echo json_encode($result);
    }

    public function get ($id)
    {
        $obj = new Usuario();
        $data = $obj->get_by_id($id);
        unset($data->senha);
        echo json_encode($data);
    }

    public function remove ($id)
    {
        $obj = new Usuario();
        $result = $obj->remove($id);
        echo json_encode(array("status" => $result > 0));
    }
}
