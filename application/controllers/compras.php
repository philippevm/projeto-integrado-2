<?php
// Inclui o arquivo cbase que possui a classe CBase
require_once dirname(__FILE__) . '/cbase.php';

// Esse é o controller, todas as ações que tiverem /comprar na URL acessada irão cair neste arquivo e executar
// as regras definidas nos códigos abaixo
//
// Define a classe compra estendendo da classe CBase encontrada no arquivo acima
class Compras extends CBase
{
    // Função que é executada antes de todas quando a classe Compras é instânciada
    public function __construct()
    {
        // Executa a função inicial da classe CBase
        parent::__construct();
    }

    public function index()
    {
        // Define a variáveil $dados como array (vetor)
        $dados = array('title' => 'Listagem de Compras');

        // Cria o objeto de compra
        $compra = new Compra();

        // Recupera todas as compras do banco de dados
        $dados['compras'] = $compra->recuperar();

        // Faz um laço de repetição em todas as compras
        foreach ($dados['compras'] as $key => $compra) {
            // $key => chave do vetor relacionado a compra atual do laço
            // $compra => objeto compra do banco

            // Instancia o Fornecedor
            $fornecedor = new Fornecedor();
            // Busca o fornecedor relativo ao id selecionado na compra
            $dados['compras'][$key]->fornecedor = $fornecedor->get_by_id($compra->fornecedor_id);

            // Instancia o Produto
            $produto = new Produto();
            // Busca o produto relativo ao id selecionado na compra
            $dados['compras'][$key]->produto = $produto->get_by_id($compra->produto_id);
        }

        // Escreve o HTML da view compras/index com os $dados fornecidos
        $this->template->write_view('conteudo', 'compras/index', $dados);
        // Renderiza a view (Mostra no navegador)
        $this->template->render();
    }

    public function cadastrar()
    {
        // Define a variáveil $dados como array (vetor)
        $dados = array('title' => 'Cadastrar Compra');

        // Cria o objeto de compra
        $obj = new Compra();

        // Busca todos os fornecedores cadastrados para popular o Select de fornecedores
        $fornecedor = new Fornecedor();
        $dados['fornecedores'] = $fornecedor->recuperar();

        // Bucas todos os produtos cadastrados para popular o Select de produtos
        $produto = new Produto();
        $dados['produtos'] = $produto->recuperar();

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Pega todos os campos do formulário pelo nome (name="") e atribui aos atributos do objeto Compra automaticamente
            $obj->setFrom($_POST);
            // Realiza a validação do objeto

            $dados['valid'] = $obj->validate();
            // Checa se o objeto é válido ou não após a validação
            if ($dados['valid']) {
                // Limpa o "R$" do valor do preço, por algum motivo o plugin de máscara não remove o R$ e deixa o valor
                // decimal inválido, então substituimos o R$ por vazio
                $obj->preco = str_replace("R$ ", "", $obj->preco);

                // Cria a compra no banco de dados
                $dados['status'] = $obj->create() > 0;

                // Redireciona para o método index do controller compras
                redirect('/compras', 'refresh');

                // Para a execução
                exit;
            } else {
                $dados['errors'] = validation_errors();
            }
        }

        // Define a chave compra do array $dados como o objeto criado de Compra
        $dados['compra'] = $obj;

        // Manda adicionar um javascript na view
        $this->template->add_js(js_url() . 'compras/manter.js');
        // Escreve o HTML com os $dados fornecidos
        $this->template->write_view('conteudo', 'compras/manter', $dados);
        // Renderiza a view
        $this->template->render();
    }

    public function editar($id)
    {
        // Define a variáveil $dados como array (vetor)
        $dados = array('title' => 'Editar Compra');

        // Cria o objeto de compra
        $obj = (new Compra())->get_by_id($id);

        // Busca todos os fornecedores cadastrados para popular o Select de fornecedores
        $fornecedor = new Fornecedor();
        $dados['fornecedores'] = $fornecedor->recuperar();

        // Bucas todos os produtos cadastrados para popular o Select de produtos
        $produto = new Produto();
        $dados['produtos'] = $produto->recuperar();

        // Checa se é POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            // Cria uma nova compra
            $compra = new Compra();
            // Define as alterações vindas do formulário no objeto
            $compra->setFrom($_POST);

            // Define manualmente o id, o preço, o produto e a quantidade para garantir que não sejam sobreescritos
            $compra->id = $id;
            $compra->preco = $obj->preco;
            $compra->produto_id = $obj->produto_id;
            $compra->quantidade = $obj->quantidade;

            // Salva a compra no banco de dados
            $dados['status'] = $compra->updateSpecific() > 0;

            // Redireciona para o método index do controller comprar
            redirect('/compras', 'refresh');

            // Para o script
            exit;
        }

        // Define a chave compra do array $dados como o objeto criado de Compra
        $dados['compra'] = $obj;

        // Manda adicionar um javascript na view
        $this->template->add_js(js_url() . 'compras/manter.js');
        // Escreve o HTML com os $dados fornecidos
        $this->template->write_view('conteudo', 'compras/manter', $dados);
        // Renderiza a view
        $this->template->render();
    }
}
