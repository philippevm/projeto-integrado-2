<?php
require_once dirname(__FILE__) . '/cbase.php';

class Produtos extends CBase {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$dados = array('title' => 'Gerenciamento de Produtos');

		$this->template->add_js(js_url() . 'produtos/index.js');
		$this->template->write_view('conteudo', 'produtos/index', $dados);
		$this->template->render();
	}

	public function ajaxListar ()
	{
		$obj = new Produto();
		$list = $obj->get_datatables();
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $product) {
			$data[] = array(
				$product->id,
				$product->nome,
				$product->marca,
				$product->preco,
				$this->load->view('produtos/botoes', array('id' => $product->id), true)
			);
			$no++;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $obj->count_all(),
			"recordsFiltered" => $obj->count_filtered(),
			"data" => $data
		);

		echo json_encode($output);
	}

	public function save ()
	{
		$result = array('status' => false, 'valid' => false, 'errors' => array());
		$obj = new Produto();
		$obj->setFrom($_POST);

		$result['valid'] = $obj->validate();
		if($result['valid']) {
			if ($obj->{$obj->__dm['primaria']}) {
				$result['status'] = $obj->updateSpecific() > 0;
			} else {
				$result['status'] = $obj->create() > 0;
			}
		} else {
			$result['errors'] = validation_errors();
		}

		echo json_encode($result);
	}

	public function get ($id)
	{
		$obj = new Produto();
		$data = $obj->get_by_id($id);
		// Busca a quantidade disponível do produto
		$data->quantidade = $obj->getAvailableQuantity($id);

		echo json_encode($data);
	}

	public function remove ($id)
	{
		$obj = new Produto();
		$result = $obj->remove($id);
		echo json_encode(array("status" => $result > 0));
	}
}
