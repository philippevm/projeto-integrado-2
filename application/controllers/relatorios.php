<?php
require_once dirname(__FILE__) . '/cbase.php';

class Relatorios extends CBase {

    public function __construct() {
        parent::__construct();
    }

    public function estoque ()
    {
        $dados = array('title' => 'Relatório de Estoque');

        $this->template->add_js(js_url() . 'relatorios/estoque.js');
        $this->template->write_view('conteudo', 'relatorios/estoque', $dados);
        $this->template->render();
    }

    public function vendas ()
    {
        $dados = array('title' => 'Relatório de Vendas');

        $this->template->add_js(js_url() . 'relatorios/vendas.js');
        $this->template->write_view('conteudo', 'relatorios/vendas', $dados);
        $this->template->render();
    }

    public function ajaxEstoque ()
    {
        $obj = new Produto();
        $list = $obj->get_datatables();
        $data = array();
        $no = $_POST['start'];

        foreach ($list as $product) {
            $total = $obj->getAvailableQuantity($product->id);

            $data[] = array(
                $product->id,
                $product->nome,
                $product->marca,
                $total
            );
            $no++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $obj->count_all(),
            "recordsFiltered" => $obj->count_filtered(),
            "data" => $data
        );

        echo json_encode($output);
    }

    public function ajaxVendas ()
    {
        $obj = new Venda();
        $list = $obj->get_datatables();
        $data = array();
        $no = $_POST['start'];

        $cliente = new Cliente();
        $itens = new Itens();

        foreach ($list as $venda) {

            $c = $cliente->get_by_id($venda->cliente_id);
            $itens->venda_id = $venda->id;
            $i = $itens->total();

            $ex = explode('-', $venda->data);

            $data[] = array(
                $venda->id,
                $ex[2].'/'.$ex[1].'/'.$ex[0],
                $c->nome,
                $i,
                $venda->total
            );
            $no++;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $obj->count_all(),
            "recordsFiltered" => $obj->count_filtered(),
            "data" => $data
        );

        echo json_encode($output);
    }
}
