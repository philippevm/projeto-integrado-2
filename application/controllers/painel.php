<?php
require_once dirname(__FILE__) . '/cbase.php';

class Painel extends CBase {

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $dados = array('title' => 'Painel');
        $this->template->write_view('conteudo', 'painel/index', $dados);
        $this->template->render();
    }

    public function venda()
    {
        $dados = array('title' => 'Realizar Venda');

        $cliente = new Cliente();
        $dados['clientes'] = $cliente->recuperar();

        $produto = new Produto();
        $dados['produtos'] = $produto->recuperar();

        $this->template->add_js(js_url() . 'painel/venda.js');
        $this->template->write_view('conteudo', 'painel/venda', $dados);
        $this->template->render();
    }

    public function ajaxVenda() {

        $obj = new Venda();
        $obj->setFrom($this->getFormattedSale());
        $venda = $obj->create();
        $total = 0;

        foreach($_POST['produtos'] as $p) {
            $itens = new Itens();
            $itens->setFrom([
                'produto_id' => $p['id'],
                'valor' => $p['preco'],
                'quantidade' => $p['qnt'],
                'desconto' => '0.00'
            ]);

            $itens->venda_id = $venda;
            $total += ($p['preco'] * $p['qnt']);
            $itens->create();
        }

        $obj->total = $total;
        $obj->id = $venda;
        $update = $obj->updateSpecific();

        echo json_encode(array(
            "check" => ($update > 0)
        ));
    }

    private function getFormattedProduct($product) {

        $total = $produto->getAvailableQuantity($product['id']);

        return array(
            'produto_id' => $product['id'],
            'valor' => $product['preco'],
            'quantidade' => $total
        );
    }

    private function getFormattedSale() {
        $post = $_POST;

        return array(
            'data' => date('Y-m-d'),
            'usuario_id' => $_SESSION['usuario']->id,
            'cliente_id' => $post['cliente']
        );
    }
}
