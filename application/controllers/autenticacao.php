<?
require_once dirname(__FILE__) . '/cbase.php';

class Autenticacao extends CBase {

	private $ent_usuario;
	private $id;
	private $login;
	private $senha;
	private $email;
	private $link_inicial;

	public function __construct() {

		parent::__construct();

		$this->ent_usuario = 'Usuario';
		$this->id = 'id';
		$this->login = 'login';
		$this->senha = 'senha';
		$this->email = 'email';
		$this->link_inicial = site_url() . '/painel/';

		$this->iniciaSessao();
	}

	public function index() {
		redirect('autenticacao/form', 'refresh');
	}

	public function form($invalida = false) {

		$this->lang->load('autenticacao');
		$dados['mensagem'] = $invalida ? $this->lang->line('autenticacao_senha_incorreta') : '';

		$this->template->set_template('autenticacao');
		$this->template->write_view('conteudo', 'autenticacao/login', $dados);
		$this->template->render();
	}

	public function login() {

		$usuario = new $this->ent_usuario;
		$usuario->{$this->login} = $this->input->post('login');
		$usuario->{$this->senha} = md5($this->input->post('senha'));
		$r = $usuario->recuperar();

		// senha correta
		if (count($r) == 1) {
			$_SESSION['usuario'] = $r[0];
			$redireciona = isset($_SESSION['url']) ? $_SESSION['url'] : $this->link_inicial;
		} else {
			$redireciona = site_url() . '/autenticacao/form/true';
		}

		redirect($redireciona, 'refresh');
	}

	public function logoff() {
		session_destroy();
		redirect('autenticacao/form', 'refresh');
	}
}
