<div class="col-md-6">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Seleção de Cliente</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="col-md-12" style="margin-bottom:10px;">
                <select id="select-clientes">
                    <option value=""></option>
                    <?php if (count($clientes) > 0): ?>
                        <?php foreach ($clientes as $cliente): ?>
                            <option value="<?php echo $cliente->id; ?>"><?php echo $cliente->nome; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12">
                <button type="button" id="btn-cliente" data-loading-text="Carregando cliente, aguarde..." class="btn default" onclick="Venda.selectClient()">Selecionar Cliente</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<div class="col-md-6">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Seleção de produtos</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="col-md-12" style="margin-bottom:10px;">
                <select id="select-produtos">
                    <option value=""></option>
                    <?php if (count($produtos) > 0): ?>
                        <?php foreach ($produtos as $produto): ?>
                            <option value="<?php echo $produto->id; ?>"><?php echo $produto->nome; ?></option>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
                <div class="clearfix"></div>
            </div>
            <div class="col-md-12">
                <button type="button" id="btn-produto" data-loading-text="Carregando produto, aguarde..." class="btn default" onclick="Venda.selectProduct()">Selecionar Produto</button>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>



<div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">Informações Gerais do Pedido</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="col-md-9">
                <div class="table-scrollable">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> ID </th>
                                <th> Nome Produto </th>
                                <th width="10%"> Quantidade </th>
                                <th> Preço Und. </th>
                                <th> Total </th>
                                <th width="10%"> Ação </th>
                            </tr>
                        </thead>
                        <tbody id="products">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-3">
                <div class="well">
                    <h2>Total:</h2>
                    <h1>R$ <span id="total">0,00</span></h1>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<div class="col-md-12" id="cliente" style="display:none;">
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">Dados do cliente selecionado</span>
            </div>
            <div class="actions"></div>
        </div>
        <div class="portlet-body">
            <div class="panel panel-default" style="margin-bottom:5px;">
                <div class="panel-heading">
                    <h3 class="panel-title" id="cliente-nome"></h3>
                </div>
                <div class="panel-body" id="cliente-dados"></div>
            </div>
        </div>
    </div>
</div>

<div class="col-md-12">
    <div class="portlet light ">
        <div class="portlet-body">
            <button class="btn btn-lg red pull-right" onclick="Venda.finalize()">Finalizar a compra!</button>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

 <div id="loading" class="modal fade" tabindex="-1" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="alert alert-warning" style="margin-bottom: 0px;" id="loading-wait">
                    Realizando venda. Aguarde...
                </div>
                <div class="alert alert-success" style="margin-bottom: 0px;display:none;" id="loading-success">
                    <strong>Successo!</strong> A venda foi realizada.
                    <p>Aguarde o redirecionamento.</p>
                </div>
            </div>
        </div>
    </div>
</div>
