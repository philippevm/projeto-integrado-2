<!-- BEGIN LOGIN FORM -->
<form class="login-form" action="/autenticacao/login" method="post">
    <h3 class="form-title font-green">Autenticação</h3>
    <div class="alert alert-danger <?php echo (empty($mensagem)) ? 'display-hide' : '' ;?>"><span><?php echo $mensagem; ?></span></div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Usuário</label>
        <input maxlength="10" class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Usuário" name="login" /> </div>
    <div class="form-group">
        <label class="control-label visible-ie8 visible-ie9">Senha</label>
        <input maxlength="12" class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Senha" name="senha" /> </div>
    <div class="form-actions">
        <button type="submit" class="btn green uppercase">Entrar</button>
    </div>
</form>
<!-- END LOGIN FORM -->
