<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Gerenciamento de Usuários</span>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <button id="sample_editable_1_new" onclick="Obj.create();" class="btn sbold green"> Cadastrar Usuário
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row"></div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_obj" style='width:100%;'>
                <thead>
                    <tr>
                        <th> ID </th>
                        <th> Nome </th>
                        <th> Usuário </th>
                        <th> E-mail </th>
                        <th> Perfil </th>
                        <th width="10%"> Ações </th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>

<div class="modal fade" id="obj" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id='alert-error' class="alert alert-block alert-danger hidden">
                    <h4 class="alert-heading">Ops. Algo de errado aconteceu.</h4>
                    <ul id='alert-errors'>
                    </ul>
                </div>

                <form id="form-obj" role="form">
                    <input type="hidden" name="id">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Nome</label>
                            <input maxlength="50" type="text" name="nome" class="form-control" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <label>Login</label>
                            <input maxlength="10" type="text" name="login" class="form-control" placeholder="Login">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input maxlength="50" type="text" name="email" class="form-control" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <label>Senha</label>
                            <input maxlength="12" type="password" name="senha" class="form-control" placeholder="Senha">
                        </div>
                        <div class="form-group">
                            <label>Perfil</label>
                            <select class="form-control" name="perfil">
                                <option value="1">Administrador</option>
                                <option value="2" selected>Vendedor</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="Obj.cancel();" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                <button type="button" id="save" onclick="$('#form-obj').submit();" class="btn green">Salvar Usuário</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirmar Exclusão</h4>
            </div>
            <div class="modal-body">
                <p>Tem certeza que deseja excluir este registro?</p>
            </div>
            <div class="modal-footer">
                <button class="btn default" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                <button id="btn-confirm" onclick="Obj.remove(this)" class="btn blue">Confirmar</button>
            </div>
        </div>
    </div>
</div>
