<style>
    .table-toolbar {
        margin-bottom: 0px;
    }

    .portlet.light .dataTables_wrapper .dt-buttons {
        margin-top: 0px;
        margin-bottom: 15px;
    }
</style>
<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Gerenciamento de Estoque</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row"></div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_obj" style='width:100%;'>
                <thead>
                    <tr>
                        <th> ID </th>
                        <th> Nome </th>
                        <th> Marca </th>
                        <th> Quantidade </th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
