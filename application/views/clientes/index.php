<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase"> Gerenciamento de Clientes</span>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <button id="sample_editable_1_new" onclick="Obj.create();" class="btn sbold green"> Cadastrar Cliente
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row"></div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_obj" style='width:100%;'>
                <thead>
                    <tr>
                        <th> ID </th>
                        <th> Nome </th>
                        <th> Telefone </th>
                        <th> E-mail </th>
                        <th> CPF</th>
                        <th> RG</th>
                        <th width="10%"> Ações </th>
                    </tr>
                </thead>
                <tbody></tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>

<div class="modal fade" id="obj" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">

                <div id='alert-error' class="alert alert-block alert-danger hidden">
                    <h4 class="alert-heading">Ops. Algo de errado aconteceu.</h4>
                    <ul id='alert-errors'>
                    </ul>
                </div>

                <form id="form-obj" role="form">
                    <input type="hidden" name="id">
                    <div class="form-body">
                        <div class="form-group">
                            <label>Nome</label>
                            <input maxlength="50" type="text" name="nome" class="form-control" placeholder="Nome">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6" style="padding-left:0px;">
                                <label>CPF</label>
                                <input maxlength="15" type="text" name="cpf" class="form-control" placeholder="CPF">
                            </div>
                            <div class="col-md-6" style="padding-right:0px;">
                                <label>RG</label>
                                <input maxlength="15" type="text" name="rg" class="form-control" placeholder="RG">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label>Telefone</label>
                            <input maxlength="15" type="text" name="telefone" class="form-control" placeholder="Telefone">
                        </div>
                        <div class="form-group">
                            <label>E-mail</label>
                            <input maxlength="50" type="text" name="email" class="form-control" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <label>Endereço Completo</label>
                            <input maxlength="255" type="text" name="endereco" class="form-control" placeholder="Endereço">
                        </div>
                        <div class="form-group">
                            <div class="col-md-6" style="padding-left:0px;">
                                <label>Número</label>
                                <input maxlength="4" type="text" name="numero" class="form-control" placeholder="Número">
                            </div>
                            <div class="col-md-6" style="padding-right:0px;">
                                <label>Bairro</label>
                                <input maxlength="100" type="text" name="bairro" class="form-control" placeholder="Bairro">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6" style="padding-left:0px;">
                                <label>Cidade</label>
                                <input maxlength="100" type="text" name="cidade" class="form-control" placeholder="Cidade">
                            </div>
                            <div class="col-md-6" style="padding-right:0px;">
                                <label>Estado</label>
                                <input maxlength="2" type="text" name="estado" class="form-control" placeholder="Estado">
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="Obj.cancel();" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                <button type="button" id="save" onclick="$('#form-obj').submit();" class="btn green">Salvar Cliente</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div id="modal-confirm" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Confirmar Exclusão</h4>
            </div>
            <div class="modal-body">
                <p>Tem certeza que deseja excluir este registro?</p>
            </div>
            <div class="modal-footer">
                <button class="btn default" data-dismiss="modal" aria-hidden="true">Cancelar</button>
                <button id="btn-confirm" onclick="Obj.remove(this)" class="btn blue">Confirmar</button>
            </div>
        </div>
    </div>
</div>
