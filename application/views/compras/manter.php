<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">Gerenciamento de Compras de Produtos de Fornecedores</span>
            </div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
            <form id="form-obj" role="form" method="POST" action="/compras/<?php echo (empty($compra->id)) ? 'cadastrar' : 'editar/'.$compra->id ?>">

                <?php if (isset($valid) && !$valid): ?>
                <div id='alert-error' class="alert alert-block alert-danger">
                    <h4 class="alert-heading">Ops. Algo de errado aconteceu.</h4>
                    <ul id='alert-errors'>
                        <?php foreach($errors as $error): ?>
                            <li><?php echo $error;?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <?php endif; ?>

                <input type="hidden" name="id" value="<?php echo $compra->id; ?>">
                <div class="form-body">
                    <div class="form-group">
                        <label>Descrição</label>
                        <input maxlength="50" type="text" name="descricao" class="form-control" placeholder="Descrição" value="<?php echo $compra->descricao; ?>">
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="padding-left:0px;">
                            <div class="form-group">
                                <label>Fornecedor</label>
                                <select class="form-control" name="fornecedor_id">
                                    <option value="">Selecione...</option>
                                    <?php if(count($fornecedores) > 0): ?>
                                        <?php foreach($fornecedores as $fornecedor): ?>
                                            <?php $opt = ($fornecedor->id == $compra->fornecedor_id) ? 'selected' : ''; ?>
                                            <option <?php echo $opt; ?> value="<?php echo $fornecedor->id; ?>"><?php echo $fornecedor->nome; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6" style="padding-right:0px;">
                            <div class="form-group">
                                <label>Produto</label>
                                <select class="form-control" name="produto_id" <?php echo ($compra->id) ? 'disabled' : '' ?>>
                                    <option value="">Selecione...</option>
                                    <?php if(count($produtos) > 0): ?>
                                        <?php foreach($produtos as $produto): ?>
                                            <?php $opt = ($produto->id == $compra->produto_id) ? 'selected' : ''; ?>
                                            <option <?php echo $opt; ?> value="<?php echo $produto->id; ?>"><?php echo $produto->nome; ?></option>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6" style="padding-left:0px;">
                            <label>Quantidade</label>
                            <input maxlength="5" type="text" name="quantidade" class="form-control" placeholder="Quantidade adquirida" value="<?php echo $compra->quantidade; ?>" <?php echo ($compra->id) ? 'disabled' : '' ?>>
                        </div>
                        <div class="col-md-6" style="padding-right:0px;">
                            <label>Preço</label>
                            <input maxlength="10" type="text" name="preco" class="form-control" placeholder="Preço Unitário" value="<?php echo $compra->preco; ?>" <?php echo ($compra->id) ? 'disabled' : '' ?>>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="row">
                    <button type="button" onclick="location.href='/compras'" class="btn dark btn-outline" data-dismiss="modal">Cancelar</button>
                    <button type="submit" id="save" class="btn green">Salvar Compra</button>
                </div>
            </form>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
