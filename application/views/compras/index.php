<div class="col-md-12">
    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light ">
        <div class="portlet-title">
            <div class="caption font-dark">
                <i class="icon-settings font-dark"></i>
                <span class="caption-subject bold uppercase">Gerenciamento de Compras de Produtos de Fornecedores</span>
            </div>
            <div class="actions">
                <div class="btn-group">
                    <button id="sample_editable_1_new" onclick="location.href='/compras/cadastrar'" class="btn sbold green"> Cadastrar Compra
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
            </div>
        </div>
        <div class="portlet-body">
            <div class="table-toolbar">
                <div class="row"></div>
            </div>
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="table_obj" style='width:100%;'>
                <thead>
                    <tr>
                        <th> ID </th>
                        <th> Descrição </th>
                        <th> Quantidade </th>
                        <th> Preço </th>
                        <th> Fornecedor</th>
                        <th> Produto </th>
                        <th width="10%"> Ações </th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($compras) == 0): ?>
                        <tr><td colspan="7">Nenhuma compra realizada.</td></tr>
                    <?php else: ?>
                        <?php foreach($compras as $compra): ?>
                            <tr>
                                <td><?php echo $compra->id; ?></td>
                                <td><?php echo $compra->descricao; ?></td>
                                <td><?php echo $compra->quantidade; ?></td>
                                <td><?php echo $compra->preco; ?></td>
                                <td><?php echo $compra->fornecedor->nome; ?></td>
                                <td><?php echo $compra->produto->nome ?></td>
                                <td>
                                    <a href="/compras/editar/<?php echo $compra->id; ?>" class="btn btn-xs primary"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
