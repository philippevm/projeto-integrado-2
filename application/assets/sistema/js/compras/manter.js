var table = $('#table_obj');
var object = 'produtos';

var Obj = function () {

    var handleMasks = function ()  {
        Inputmask.extendAliases({
            "brl": {
                prefix: "R$ ",
                groupSeparator: ".",
                alias: "numeric",
                placeholder: "0",
                autoGroup: !0,
                digits: 2,
                digitsOptional: !1,
                clearMaskOnLostFocus: !1,
                autoUnmask: true
            }
        });

        $('[name="preco"]').inputmask({
            alias: 'brl',
            autoUnmask: true
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            handleMasks();
        }
    };
}();

jQuery(document).ready(function() {
    Obj.init();
});
