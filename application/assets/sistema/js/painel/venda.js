var products = [];

var Venda = function() {

    var handleSelects = function() {

        $.fn.select2.defaults.set("theme", "bootstrap");

        $("#select-produtos").select2({
            placeholder: "Selecione um produto",
            width: null
        });

        $("#select-clientes").select2({
            placeholder: "Selecione um cliente",
            width: null
        });
    }

    var toggleButton = function (selector, method) {

        var btn = $(selector);
        if (!method) {
            btn.button('loading');
            return;
        }
        btn.button('reset');
    }

    var generateCustomerData = function (data) {

        var html = "";
        html += "<p><strong>Telefone:</strong> "+data.telefone+"</p>";
        html += "<p><strong>RG:</strong> "+data.rg+"</p>";
        html += "<p><strong>CPF:</strong> "+data.cpf+"</p>";
        html += "<p><strong>Endereço Completo:</strong> "+data.endereco+", "+data.numero+", "+data.bairro+" - "+data.cidade+", "+data.estado+"</p>";

        return html;
    }

    var selectClient = function () {

        var client = $('#select-clientes').val();
        if (client == "") {
            toastr.error('Você precisa selecionar um cliente!');
            return false;
        }

        toggleButton('#btn-cliente');

        $.ajax({
                url : "/clientes/get/"+client,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    $('#cliente-nome').html(data.nome + " - " + data.email);
                    $('#cliente-dados').html(generateCustomerData(data));
                    $('#cliente').show();
                    toastr.success('Cliente selecionado com sucesso!');
                }
        }).done(function() {
            toggleButton('#btn-cliente', true);
        });

        return true;
    }

    var calculatePrice = function (price, qnt) {
        return parseFloat(price) * parseInt(qnt);
    }

    var reloadTable = function () {
        var html = "";
        if(products.length > 0) {
            $.each(products, function () {
                html += generateProductData(this);
            });
        } else {
            html = '<tr><td colspan="6" style="text-align:center;">Selecione produtos.</td></tr>';
        }

        $('#products').html(html);
    }

    var removeProduct = function (id) {
        var index = findInArray(id);
        products.splice(index, 1);

        reloadTable();
        recalculate();
    }

    var generateProductData = function (p) {

        var html = "<tr>";
        var unit = parseFloat(p.preco);
        var qnt = p.qnt || 1;
        var price = calculatePrice(unit, qnt);

        html += "<td>"+p.id+"</td>";
        html += "<td>"+p.nome+"</td>";
        html += "<td><input type='text' class='form-control input-sm input-xsmall' name='p_"+p.id+"' value='"+qnt+"'></td>";
        html += "<td>"+unit.formatMoney(2, ',', '.')+"</td>";

        html += "<td>"+price.formatMoney(2, ',', '.') +"</td>";
        html += "<td><button class='btn red btn-xs' onclick='Venda.removeProduct("+p.id+")'>Remover</button></td>";
        html += "</tr>";

        return html;
    }

    var recalculate = function () {
        var total = 0;
        $.each(products, function () {
            var qnt = this.qnt || 1;
            total += calculatePrice(this.preco, qnt);
        });

        var formatted = (total).formatMoney(2, ',', '.');
        $('#total').html(formatted);
    }

    var selectProduct = function () {

        var product = $('#select-produtos').val();
        if (product == "") {
            toastr.error('Você precisa selecionar produto!');
            return false;
        }

        toggleButton('#btn-produto');

        $.ajax({
                url : "/produtos/get/"+product,
                type: "GET",
                dataType: "JSON",
                success: function(data) {

                    if (data.quantidade > 0) {
                        data.qnt = 1;
                        products.push(data);
                        reloadTable();
                        recalculate();
                        return;
                    }

                    toastr.error('Este produto está em falta no estoque!');
                }
        }).done(function() {
            $('#select-produtos').select2("val", "");
            toggleButton('#btn-produto', true);
        });
    }

    var findInArray = function (id) {
        var index;
        $.each(products, function (key, value) {
            if(value.id == id) {
                index = key;
            }
        });
        return index;
    }

    var handleQntInputs = function () {

        $('body').on('change', 'input[name^="p_"]', function (obj) {
            var qnt = $(this).val();
            var id = parseInt($(this).attr('name').replace('p_', ''));

            if(qnt != 0) {

                var index = findInArray(id);
                products[index].qnt = parseInt(qnt);

                reloadTable();
                recalculate();
                return;
            }

            toastr.error('A quantidade deve ser maior que 0!');
        });
    }

    var finalize = function () {

        var check = true;
        var cliente = $('#select-clientes').select2('val');
        if(cliente == "") {
            toastr.error('Um cliente deve ser selecionado!');
            check = false;
        }

        if(products.length == 0) {
            toastr.error('É necessário selecionar ao menos um produto!');
            check = false;
        }

        if(check) {

            $('#loading').modal('show');

            $.ajax({
                    url : "/painel/ajaxVenda",
                    type: "POST",
                    dataType: "JSON",
                    data: {
                        produtos: products,
                        cliente: cliente
                    },
                    success: function(data) {
                        if(data.check) {

                            $('#loading-wait').hide();
                            $('#loading-success').show();

                            setTimeout(function() {
                                location.href = '/painel';
                            }, 2000);

                        } else {
                            toastr.error('Algo de errado aconteceu durante a transação.');
                            $('#loading').modal('hide');
                        }
                    }
            });

            return;
        }

        $('#loading').modal('hide');
    }

    return {
        //main function to initiate the module
        init: function() {
            handleSelects();
            handleQntInputs();
            reloadTable();
        },
        selectClient: selectClient,
        selectProduct: selectProduct,
        removeProduct: removeProduct,
        finalize: finalize
    };
}();

$(document).ready(function() {
    Venda.init();
});

Number.prototype.formatMoney = function(c, d, t){
    var n = this,
    c = isNaN(c = Math.abs(c)) ? 2 : c,
    d = d == undefined ? "." : d,
    t = t == undefined ? "," : t,
    s = n < 0 ? "-" : "",
    i = String(parseInt(n = Math.abs(Number(n) || 0).toFixed(c))),
    j = (j = i.length) > 3 ? j % 3 : 0;
   return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
