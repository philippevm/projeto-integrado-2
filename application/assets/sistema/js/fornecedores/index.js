var table = $('#table_obj');
var object = 'fornecedores';

var Obj = function () {
    
    var init = function () {
        // begin first table
        table.dataTable({
            "ajax": {
                "url": "/"+object+"/ajaxListar",
                "type": "POST"
            },
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "Nenhum registro encontrado.",
                "info": "Mostrando _START_-_END_ do total de _TOTAL_ registros.",
                "infoEmpty": "Nenhum registro encontrado.",
                "infoFiltered": "Filtrado de _MAX_ registro(s).",
                "lengthMenu": "_MENU_",
                "search": "Buscar:",
                "zeroRecords": "Nenhum registro encontrado pelo filtro informado.",
                "paginate": {
                    "previous":"Anterior",
                    "next": "Próximo",
                    "last": "Último",
                    "first": "Primeiro"
                }
            },
            "serverSide": true,
            "processing": true,
            "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
            "columnDefs": [ {
                "targets": 0,
                "orderable": false,
                "searchable": false
            }],
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"] // change per page values here
            ],
            // set the initial value
            "pageLength": 10,            
            "pagingType": "bootstrap_full_number",
            "order": [] // set first column as a default sort by asc
        });
    }

    function edit (id) {

        $.ajax({
            url : "/"+object+"/get/" + id,
            type: "GET",
            dataType: "JSON",
            success: function(data)
            {
                setTitle('Editar Fornecedor');
                $.each(data, function(key, value) { 
                    $('[name="'+key+'"]').val(value);    
                });
                $('#obj').modal('show');
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                toastr.error('Erro ao recuperar dados do ajax.');
            }
        });
    }

    function cancel () {
        reset();
    }

    function reset () {
        $('#form-obj input').val("");
        $('#form-obj select').val(2);
    }

    function create () {
        this.setTitle('Cadastrar Fornecedor');
        $('#obj').modal('show');
    }

    function setTitle (str) {
        $('.modal-title').html(str);
    }

    function hideModal () {
        $('#alert-error').addClass('hidden');
        $('#alert-errors').html();
        $('#obj').modal('hide');
    }

    function save () {

        $('#save').attr('disabled', true);
        $('#form-obj input, #form-obj select').attr('disabled', false);

        $.ajax({
            url : "/"+object+"/save",
            type: "POST",
            dataType: "JSON",
            data: $('#form-obj').serialize(),
            success: function(data) {
                
                if(data.valid) {
                    reload();    
                    hideModal();
                    toastr.success('Fornecedor salvo com sucesso!');
                } else {
                    var html = '';
                    $.each(data.errors, function (field, message) {
                        html += '<li>'+message+'</li>';
                    });
                    $('#alert-errors').html(html);
                    $('#alert-error').removeClass('hidden');  
                    toastr.error('Erro! Verifique as informações fornecidas.'); 
                }

                $('#form-obj input, #form-obj select').attr('disabled', false);
                $('#save').attr('disabled', false);
            }
        });
    }

    function remove (button) {

        $('#modal-confirm').modal('hide');

        var id = $(button).attr('data-id');
        if (id !== undefined)  {
            $.ajax({
                url : "/"+object+"/remove/"+id,
                type: "GET",
                dataType: "JSON",
                success: function(data) {
                    toastr.success('Fornecedor removido com sucesso!');
                }
            }).done(function() {
                reload();
            });       
        }        
    }
    
    function confirmRemove(id) {
        $('#btn-confirm').attr('data-id', id);
        $('#modal-confirm').modal('show');
    }

    function reload() {
        table._fnAjaxUpdate();
    }
    
    var handleMasks = function ()  {
        $('[name="telefone"]').inputmask("(99) 99999-9999", {
            autoUnmask: true,
            placeholder: "(__) _____-____",
            clearMaskOnLostFocus: true
        });
        $('[name="cnpj"]').inputmask("999.999.9999/99", {
            autoUnmask: true,
            placeholder: "___.___.____/__",
            clearMaskOnLostFocus: true
        });
    };

    return {
        //main function to initiate the module
        init: function () {
            if (!jQuery().dataTable) {
                return;
            }
            init();
            handleMasks();
        },
        edit: edit,
        create: create,
        save: save,
        confirmRemove: confirmRemove,
        remove: remove,
        cancel: cancel,
        setTitle: setTitle
    };
}();

jQuery(document).ready(function() {

    Obj.init();

    $('form[id="form-obj"]').submit(function () {
        Obj.save();
        return false;
    });

    $('#obj').on('hidden.bs.modal', function () {
        Obj.cancel();
    });
}); 