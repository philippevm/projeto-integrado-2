<?php
$lang['autenticacao_senha_incorreta'] = 'Senha incorreta ou usu&aacute;rio inexistente!';
$lang['autenticacao_senha_nao_confere'] = 'Senha atual n&atilde;o confere.';
$lang['autenticacao_senha_alterada'] = 'Sua senha foi alterada com sucesso.';
$lang['autenticacao_senha_alterada_email'] = 'Sua senha foi alterada com sucesso, em instantes voc&ecirc; receber&aacute; sua nova senha por e-mail.';