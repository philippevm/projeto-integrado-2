<?
require_once 'base.php';

class Usuario extends Base {

    public $columnOrder = array();

    public $columnSearch = array('nome', 'login', 'email');

    public $order = array('id' => 'desc');

    public function __construct() {
        parent::__construct();
    }

    public function validate () {

        $rules = array(
            'nome' => array(
                'title' => 'Nome',
                'rules' => 'required|min_length[5]'
            ),
            'login' => array(
                'title' => 'Login',
                'rules' => 'required|min_length[5]'
            ),
            'email' => array(
                'title' => 'E-mail',
                'rules' => 'required|trim|xss_clean|valid_email'
            ),
            'senha' => array(
                'title' => 'Senha',
                'rules' => 'md5|trim|min_length[6]',
                'depends' => array(
                    'field' => 'id',
                    'filter' => 'checkValue',
                    'rules' => 'required'
                )
            ),
            'perfil' => array(
                'title' => 'Perfil',
                'rules' => 'required'
            )
        );

        return $this->objectValidate($rules)->run();
    }
}
