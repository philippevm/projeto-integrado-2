<?
require_once dirname(__FILE__) . '/../../system/core/Model.php';

class Base extends CI_Model {

    public $__dm;

    public function __construct($tabela = null) {

        parent::__construct();
        $this->__dm['tabela'] = $tabela ? $tabela : strtolower(get_class($this)).'s';
        $this->getAtributos();
    }

    private function getAtributos(){

        $CI = &get_instance();

        $campos = $CI->db->field_data($this->__dm['tabela']);
        foreach ($campos as $campo => $atributo) {
            if ($atributo->primary_key)
                $this->__dm['primaria'] = $atributo->name;
            $this->{$atributo->name} = NULL;
        }
    }

    private function _get_datatables_query()
    {
        $this->db->from($this->__dm['tabela']);

        $i = 0;
        foreach ($this->columnSearch as $item) // loop column
        {
            if($_POST['search']['value']) // if datatable send POST for search
            {
                if($i === 0) // first loop
                {
                    $or_where = "(";
                    $or_where .= $item . " LIKE '%".$_POST['search']['value']."%'";
                }
                else
                {
                    $or_where.=" OR $item LIKE '%".$_POST['search']['value']."%'";
                }

                if(count($this->columnSearch) - 1 == $i) {
                    $or_where.=")";
                    $this->db->or_where($or_where);
                }
            }
            $i++;
        }

        if(isset($_POST['order'])) // here order processing
        {
            $this->db->order_by($this->columnOrder[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }
        else if(isset($this->order))
        {
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }

    function get_datatables()
    {
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }

    function count_filtered()
    {
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    public function count_all()
    {
        $this->db->from($this->__dm['tabela']);
        return $this->db->count_all_results();
    }

    public function get_by_id($id)
    {
        $this->db->from($this->__dm['tabela']);
        $this->db->where($this->__dm['primaria'], $id);
        $query = $this->db->get();

        return $query->row();
    }

    private function clearObject($obj)
    {
        unset($obj->__dm);
        unset($obj->table);
        unset($obj->columnOrder);
        unset($obj->columnSearch);
        unset($obj->order);
        return $obj;
    }

    // Arrumado a partir daqui
    public function create() {
        $obj = $this->clearObject(clone($this));
        $this->db->insert($this->__dm['tabela'], $obj);
        return $this->db->insert_id();
    }

    public function updateSpecific() {
        $obj = $this->clearObject(clone($this));
        $this->db->where($this->__dm['primaria'], $obj->{$this->__dm['primaria']});

        $vars = get_object_vars($obj);
        foreach ($vars as $posicao => $valor) {
            if (!isset($valor))
                unset($obj->{$posicao});
        }

        if (count(get_object_vars($obj)) > 0)
            $this->db->update($this->__dm['tabela'], $obj);

        return $this->db->affected_rows();
    }

    public function remove($id) {
        $this->db->where($this->__dm['primaria'], $id);
        $this->db->delete($this->__dm['tabela']);
        return $this->db->affected_rows();
    }

    public function condicao($where, $operador = 'and') {
        $operador = strtolower($operador);

        if ($operador == 'or') {
            $this->db->or_where($where);
        }
        else {
            $this->db->where($where);
        }
    }

    public function ordenarPor($ordenacao) {
        $this->db->order_by($ordenacao);
    }

    public function limite($limite, $inicio = 0) {
        $this->db->limit($limite, $inicio);
    }

    private function _query() {
        $obj = $this->clearObject(clone($this));
        $vars = get_object_vars($obj);

        foreach ($vars as $campo => $valor) {
            if (isset($valor) && $valor != '%') {
                if (preg_match('/%/', $valor)) {
                    $valor = str_replace('%', '', $valor);
                    $this->db->like($this->__dm['tabela'].'.'.$campo, $valor);
                }
                else
                    $this->db->where($this->__dm['tabela'].'.'.$campo, $valor);
            }
        }
    }

    public function join($tabela, $campo) {
        $this->db->join($tabela, "$tabela.$campo = {$this->__dm['tabela']}.$campo", 'left');
    }

    public function recuperar() {
        $this->_query();
        $this->db->from($this->__dm['tabela']);
        return $this->db->get()->result();
    }

    public function total() {
        $select = $this->db->ar_select;
        $this->db->ar_select = array('count(*) as total');
        $retorno = $this->recuperar();
        $this->db->ar_select = $select;

        return $retorno[0]->total;
    }

    public function setFrom($de) {

        $tipo = gettype($de);
        switch ($tipo) {
            case 'array':
                foreach ($this as $campo => $valor)
                    if (isset($de[$campo]) && $de[$campo])
                        $this->$campo = $de[$campo];
                break;
            case 'object':
                foreach ($this as $campo => $valor)
                    if (isset($de->$campo) && $de->$campo)
                        $this->$campo = $de->$campo;
                break;
        }
    }

    public function iniciaTransacao(){
        $this->db->trans_start();
    }

    public function finalizaTransacao(){
        $this->db->trans_complete();
    }

    private function checkValue ($str) {
        return empty($str);
    }

    public function objectValidate ($rules) {
        $this->load->library('form_validation', '', 'validator');

        foreach($rules as $field => $array) {

            if (isset($array['depends'])) {

                $depends = $array['depends'];
                $field = $this->{$depends['field']};

                if($this->{$depends['filter']}($field)) {
                    $array['rules'] .= "|".$depends['rules'];
                }
            }

            $this->validator->set_rules($field, $array['title'], $array['rules']);
        }

        return $this->validator;
    }

    public function executeQuery($query) {
        $query = $this->db->query($query);
        return $query->result_array();
    }
}
