<?
require_once 'base.php';

class Itens extends Base {

    public function __construct() {
        parent::__construct('vendas_inter_produtos');
    }

    public function validate () {

        $rules = array();

        return $this->objectValidate($rules)->run();
    }
}
