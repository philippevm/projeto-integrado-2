<?
require_once 'base.php';

class Venda extends Base {

    public $columnOrder = array();

    public $columnSearch = array('data', 'total');

    public $order = array('id' => 'desc');

    public function __construct() {
        parent::__construct();
    }

    public function validate () {

        $rules = array();

        return $this->objectValidate($rules)->run();
    }
}
