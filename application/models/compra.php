<?php
// Inclui o arquivo base que possui a classe Base
require_once 'base.php';

// Esse é o model, está vinculado ao banco de dados, a classe Base provê funções de ajuda para inserir, editar, deletar
// e buscar no banco
//
// Define a classe compra estendendo da classe Base encontrada no arquivo acima
class Compra extends Base {

    // Função que é executada quando a classe Compra é instânciada
    public function __construct() {
        // Executa a função inicial da classe Base
        parent::__construct();
    }

    // Define a função de validação do Objeto Compra
    public function validate () {

        // Cria um array (vetor) de regras para os campos do objeto compra
        $rules = array(
            'id' => array(
                'title' => 'ID',
                'rules' => 'trim'
            ),
            // Regras para o campo descrição
            // Requerido, Tamanho mínimo 10
            'descricao' => array(
                'title' => 'Descrição',
                'rules' => 'required|min_length[10]'
            ),
            // Regras para o campo preço
            // Requerido, Limpo de espaços
            'preco' => array(
                'title' => 'Preço',
                'rules' => 'required|trim'
            ),
            // Regras para o campo quantidade
            // Requerido, Limpo de espaços, Tamanho mínimo 1
            'quantidade' => array(
                'title' => 'Quantidade',
                'rules' => 'required|trim|min_length[1]'
            ),
            // Regras para o campo ID do fornecedor
            // Requerido
            'fornecedor_id' => array(
                'title' => 'Fornecedor',
                'rules' => 'required'
            ),
            // Regras para o campo ID do produto
            // Requerido
            'produto_id' => array(
                'title' => 'Produto',
                'rules' => 'required'
            )
        );

        // Executa a validação do objeto e retorna se é sucesso ou não de acordo com as regras definidas
        return $this->objectValidate($rules)->run();
    }
}
