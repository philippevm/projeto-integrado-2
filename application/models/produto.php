<?
require_once 'base.php';

class Produto extends Base {

    public $columnOrder = array();

    public $columnSearch = array('nome', 'login', 'email');

    public $order = array('id' => 'desc');

    public function __construct() {
        parent::__construct();
    }

    public function validate () {

        $rules = array(
            'nome' => array(
                'title' => 'Nome',
                'rules' => 'required|min_length[5]'
            ),
            'marca' => array(
                'title' => 'Marca',
                'rules' => 'required|min_length[5]'
            ),
            'preco' => array(
                'title' => 'Preço',
                'rules' => 'required|trim'
            )
        );

        return $this->objectValidate($rules)->run();
    }

    public function getAvailableQuantity($id) {

        $count = $this->executeQuery("SELECT SUM(quantidade) as total FROM vendas_inter_produtos WHERE produto_id = {$id}");
        $total = $this->executeQuery("SELECT SUM(quantidade) as total FROM compras WHERE produto_id = {$id}");

        return $total[0]['total'] - $count[0]['total'];
    }

    public function getTotalQuantitySold($id) {
        $count = $this->executeQuery("SELECT SUM(quantidade) as total FROM vendas_inter_produtos WHERE produto_id = {$id}");

        return $count[0]['total'];
    }
}
