<?
require_once 'base.php';

class Cliente extends Base {

	public $columnOrder = array();

	public $columnSearch = array('nome', 'telefone', 'email', 'cpf', 'rg');

	public $order = array('id' => 'desc');

	public function __construct() {
		parent::__construct();
	}

	public function validate () {
		
		$rules = array(
			'nome' => array(
				'title' => 'Nome',
				'rules' => 'required|min_length[5]'
			),
			'cpf' => array(
				'title' => 'CPF',
				'rules' => 'required|min_length[11]'
			),
			'email' => array(
				'title' => 'E-mail',
				'rules' => 'required|trim|xss_clean|valid_email'
			),
			'telefone' => array(
				'title' => 'Telefone',
				'rules' => 'required'
			)
		);

		return $this->objectValidate($rules)->run();
	}
}