<?
/**
 * Arquivo de configuracao para permissao
 *
 * permissao[id_perfil] = 'controlador/metodo' ou 'controlador/*' para todos os metodos
 *
 * Ex.:
 * $permissao[1][] = 'autenticacao/*';
 */
$permissao[1][] = 'painel/*';
$permissao[1][] = 'usuarios/*';
$permissao[1][] = 'clientes/*';
$permissao[1][] = 'fornecedores/*';
$permissao[1][] = 'produtos/*';
$permissao[1][] = 'relatorios/*';
$permissao[1][] = 'compras/*';
