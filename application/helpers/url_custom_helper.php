<?

if ( ! function_exists('assets_url')) {
	function assets_url() {
		$CI =& get_instance();
		return $CI->config->slash_item('base_url') . APPPATH . 'assets/';
	}
}

if ( ! function_exists('img_url')) {
	function img_url() {
		return assets_url() . 'sistema/img/';
	}
}

if ( ! function_exists('css_url')) {
	function css_url() {
		return assets_url() . 'sistema/css/';
	}
}

if ( ! function_exists('js_url')) {
	function js_url() {
		return APPNAME . '/assets/sistema/js/';
	}
}

if ( ! function_exists('ent_path')) {
	function ent_path() {
		return dirname(__FILE__) . '/../models/entities/';
	}
}
