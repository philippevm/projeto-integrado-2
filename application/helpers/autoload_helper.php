<?
function __autoload($classe) {
	$classe = strtolower($classe);
	if ( (!preg_match('/ci_/', strtolower($classe))) && (!preg_match('/my_/', strtolower($classe))) ) {
		require_once dirname(__FILE__) . "/../models/$classe.php";
	}
}