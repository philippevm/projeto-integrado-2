<?

function dateBrToIso($data) {
	if (isset($data) && $data) {
		$tmp = explode('/', $data);
		$data = "{$tmp[2]}-{$tmp[1]}-{$tmp[0]}";
	}

	return $data;
}


/**
 * @param dateIso $data: formato (2012-01-05 12:10:49)
 * 
 * @return dateBr: formato (05/01/2012 12:10:49)
 */
function dateIsoToBr($data) {
	if (isset($data) && $data) {
		$quebra_data_hora = explode(' ', $data);
			
		$tmp = explode('-', $quebra_data_hora[0]);
		$data = "{$tmp[2]}/{$tmp[1]}/{$tmp[0]}";
		
		$data .= isset($quebra_data_hora[1]) ? " {$quebra_data_hora[1]}" : '';
	
		return $data;
	}
}


/**
 * @param dateIso $data: formato (2012-01-05 12:10:49)
 * 
 * @return timestamp
 */
function dateIsoToTimestamp($data) {
	$quebra_data_hora = explode(' ', $data);
	$quebra_data = explode('-', $quebra_data_hora[0]);
	
	if (isset($quebra_data_hora[1]))
		$quebra_hora = explode(':', $quebra_data_hora[1]);
	else {
		$quebra_hora[0] = 0;
		$quebra_hora[1] = 0;
		$quebra_hora[2] = 0;
	}

	$timestamp = mktime($quebra_hora[0], $quebra_hora[1], $quebra_hora[2], $quebra_data[1], $quebra_data[2], $quebra_data[0]);
	
	return $timestamp;
}


/**
 * @param dateBr $data
 * 
 * @return boolean
 */
function isValidDateBr($data) {
	if ($data) {
		$quebra_data = explode('/', $data);
		
		return checkdate($quebra_data[1], $quebra_data[0], $quebra_data[2]);
	}
	
	return false;
}
