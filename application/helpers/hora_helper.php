<?
/**
 * somarMinutos: soma ou subtrai minutos no timestamp
 * 
 * @param timestamp $timestamp
 * @param int $minutos
 * 
 * @return timestamp
 */
function somarMinutos($timestamp, $minutos) {
	$hora = date('H', $timestamp);
	$minuto = date('i', $timestamp);
	$segundo = date('s', $timestamp);
	$mes = date('m', $timestamp);
	$dia = date('d', $timestamp);
	$ano = date('Y', $timestamp);
	
	return mktime($hora, $minuto + $minutos, $segundo, $mes, $dia, $ano);
}
