<?
/**
 * Verifica se um determinado script PHP esta em execucao
 * @param string $controlador: Nome do controlador do script, se nao for informado, sera utilizado 
 * 							   o proprio controlador do script em execucao
 * @param string $metodo: Nome do metodo do script, se nao for informado, sera utilizado 
 * 						  o proprio metodo do script em execucao
 *
 * @return boolean: TRUE - Em execucao / FALSE - Nao esta em execucao
 */
function verificar_execucao($controlador = null, $metodo = null) {
	
	$CI = &get_instance();
	$controlador = $controlador ? $controlador : $CI->router->fetch_class();
	$metodo = $metodo ? $metodo : $CI->router->fetch_method();

	$ret = null;
    exec("/bin/ps aux | /bin/grep '$controlador' | /bin/grep '$metodo' | /bin/grep -v grep", $ret);
	
	return (count($ret) > 1) ? true : false;
}


function escrever_log($script, $identificacao, $texto) {
	error_log("[" . date("d/m/Y H:i:s") . "][$identificacao]: $texto\n", 3, $script);
}