<?

/**
 * Converte de 1.000,00 para 1000.00
 *
 */
function signedToDecimal($signed) {
	$signed = str_replace('.', '', $signed);
	$decimal = (float)str_replace(',', '.', $signed);
	
	return $decimal;
}

/**
 * Converte de 1000.00 para 1000,00
 */
function decimalToSigned($decimal) {
	return str_replace('.', ',', sprintf("%01.2f", $decimal));
}